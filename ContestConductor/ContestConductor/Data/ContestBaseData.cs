﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContestConductor.Data
{
    public abstract class ContestBaseData
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Details { get; set; }

        public DateTime Start { get; set; }

        public TimeOnly Duration { get; set; }

        public string Author { get; set; }
    }
}
