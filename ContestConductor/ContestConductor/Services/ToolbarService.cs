﻿using ContestConductor.Tools;
using ContestConductor.WebUi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContestConductor.Services
{
    public class ToolbarService : IToolbarService
    {
        public event AsyncRequest<IToolbarButton<EditableContentMode>> EditButtonRequested;

        public async Task<IToolbarButton<EditableContentMode>> RequestEditButtonAsync()
        {
            if (EditButtonRequested != null)
            {
               return await EditButtonRequested();
            }

            return new ToolbarButtonProxy<EditableContentMode>();
        }

        private class ToolbarButtonProxy<T> : IToolbarButton<T>
        {
            public bool IsAvailable => false;

            public event AsyncEventHandler<T> OnButtonClick;
        }
    }
}
