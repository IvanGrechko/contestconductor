﻿using ContestConductor.WebUi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContestConductor.Services
{
    public class ApplicationService : IAppService
    {
        public IToolbarService ToolbarService { get; }

        public ApplicationService()
        {
            ToolbarService = new ToolbarService();
        }
    }
}
