﻿using ContestConductor.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContestConductor.Services
{
    public class StaticContestRepository : IContestRepository
    {
        private Lazy<IAsyncEnumerable<ContestData>> lazy_;

        public StaticContestRepository()
        {
            lazy_ = new Lazy<IAsyncEnumerable<ContestData>>(() =>
            {
                return new ContestData[]
                   {
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 15", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" },
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 16", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" },
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 17", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" },
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 18", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" },
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 19", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" },
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 20", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" },
                       new ContestData { Id = Guid.NewGuid().ToString(), Title = "Codeforces Global Round 21", Details = "asdsad asdas d asdasd as asdas das dsadas ", Start = DateTime.UtcNow, Duration = new TimeOnly(3, 15), Author = "testUser" }
                   }.ToAsyncEnumerable();
            });
        }

        public async Task<ContestData> GetContestAsync(string id)
        {
            return await GetAllAsync().FirstOrDefaultAsync(d => d.Id == id);
        }

        public IAsyncEnumerable<ContestShortData> GetContestListAsync()
        {
            return GetAllAsync().Select(d => new ContestShortData
            {
                Id = d.Id,
                Author = d.Author,
                Details = d.Details,
                Duration = d.Duration,
                Start = d.Start,
                Title = d.Title,
            });
        }

        protected IAsyncEnumerable<ContestData> GetAllAsync()
        {
            return lazy_.Value;
        }
    }
}
