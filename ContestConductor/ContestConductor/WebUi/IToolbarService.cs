﻿using ContestConductor.Tools;
using System.Threading.Tasks;

namespace ContestConductor.WebUi
{
    public interface IToolbarService
    {
        event AsyncRequest<IToolbarButton<EditableContentMode>> EditButtonRequested;
        Task<IToolbarButton<EditableContentMode>> RequestEditButtonAsync();
    }

    public interface IToolbarButton<T>
    {
        event AsyncEventHandler<T> OnButtonClick;

        bool IsAvailable { get; }
    }

    public enum EditableContentMode
    {
        Readonly,

        EditMode
    }
}