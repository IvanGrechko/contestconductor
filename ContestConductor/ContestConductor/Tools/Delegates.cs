﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContestConductor.Tools
{
    public delegate Task AsyncEventHandler();

    public delegate Task AsyncEventHandler<T>(T state);

    public delegate Task<TResponse> AsyncRequest<TResponse>();

}
